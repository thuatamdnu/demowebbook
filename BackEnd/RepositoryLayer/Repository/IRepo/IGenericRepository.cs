﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Query;

namespace RepositoryLayer.Repository.IRepo
{
    public interface IGenericRepository<T> where T : class, new()
    {
        //Task<T> Get(Expression<Func<T, bool>> filter = null);
        Task<List<T>> GetList(Expression<Func<T, bool>> filter = null,  Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
            Func<IQueryable<T>, IIncludableQueryable<T, object>> include = null);
        //Task<T> Add(T entity);
        //Task<T> Update(T entity);
        //Task<int> Delete(T entity);
        //Task<List<T>> AddRange(List<T> entity);
        //Task<List<T>> UpdateRange(List<T> entity);
    }
}
