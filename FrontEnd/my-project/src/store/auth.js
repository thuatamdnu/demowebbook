import Axios from "axios";

const loginUrl = "https://localhost:44398/api/User/Login";
const loginCustomerUrl = "https://localhost:44398/api/Customer/Login";
export default {
    namespaced: true,
    state: {
        checked: false,
        authenticated: false
    },
    mutations: {
        setAuthenticated(state) {
            state.authenticated = true;
        },
        setchecked(state) {
            state.checked = true;
        }
    },
    actions: {
        async authenticate(context, credentials) {
            let response = await Axios.post(loginUrl, credentials);
            if (response.data.success == true) {
                context.commit("setAuthenticated");
            }
        },
        async check(context2, credentials2) {
            let response = await Axios.post(loginCustomerUrl, credentials2);
            if (response.data.success == true) {
                context2.commit("setchecked");
            }
        }

    },
    
};
