﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.Models
{
    public class Order : BaseEntity
    {
        public int TotalMoney { get; set; }
        public string Payment { get; set; }
        public DateTime BookingDate { get; set; }
        public string DeliveryData { get; set; }
        public string ShippingWay { get; set; }
        public string State { get; set; }
        public string Note { get; set; }
        public string TransportFee { get; set; }
        public int CustomerId { get; set; }
        public virtual Customer Customer { get; set; }
        public virtual List<OrderDetail> OrderDetails { get; set; }
    }
}
