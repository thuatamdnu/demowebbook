﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.Models
{
    public class Slide : BaseEntity
    {
        public string SlideName { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public string Url { get; set; }
        public int SortOrder { get; set; }
        public string Status { get; set; }
    }
}
