﻿using DomainLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.EntityMapper
{
    public class SlideMap : IEntityTypeConfiguration<Slide>
    {
        public void Configure(EntityTypeBuilder<Slide> builder)
        {
            builder.HasKey(x => x.Id).HasName("pk_slideid");
            builder.Property(x => x.Id).ValueGeneratedOnAdd()
                .HasColumnName("Id").HasColumnType("INT");
            builder.Property(x => x.SlideName)
                .HasColumnType("NVARCHAR(50)").IsRequired();
            builder.Property(x => x.Description)
                .HasColumnType("NVARCHAR(50)").IsRequired();
            builder.Property(x => x.Image)
                .HasColumnType("NVARCHAR(50)").IsRequired();
            builder.Property(x => x.Url)
                .HasColumnType("NVARCHAR(50)").IsRequired();
            builder.Property(x => x.SortOrder)
                .HasColumnType("NVARCHAR(50)").IsRequired();
            builder.Property(x => x.Status)
                .HasColumnType("NVARCHAR(50)").IsRequired();
        }
    }
}
