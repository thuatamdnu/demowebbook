﻿using AutoMapper;
using BookStoreDTO.DTO;
using DomainLayer.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using ServicesLayer.SlideService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eShopOnWeb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SlideController : ControllerBase
    {
        private readonly ISlideService SlideService;
        private readonly IMapper _mapper;
        private readonly IWebHostEnvironment _webHostEnvironment;
        public SlideController(ISlideService SlideService, IMapper mapper)
        {
            this.SlideService = SlideService;
            _mapper = mapper;
            //_webHostEnvironment = webHostEnvironment;
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            var config = new MapperConfiguration(cfg => cfg.CreateMap<Slide, SlideDTO>().ReverseMap());
            _mapper = new Mapper(config);
        }
        [HttpGet(nameof(GetSlide))]
        public IActionResult GetSlide(int id)
        {
            var result = SlideService.GetSlide(id);
            if (result is not null)
            {
                return Ok(result);
            }
            return BadRequest("Not found");
        }
        [HttpGet(nameof(GetAllSlide))]
        public IActionResult GetAllSlide()
        {
            var result = SlideService.GetAllSlide();
            var SlideDTO = _mapper.Map<IEnumerable<SlideDTO>>(result);
            if (SlideDTO is not null)
            {
                return Ok(SlideDTO);
            }
            return BadRequest("Not Found");
        }
        [HttpPost(nameof(InsertSlide))]
        public IActionResult InsertSlide(SlideDTO slideDTO)
        {
            var slide = _mapper.Map<Slide>(slideDTO);
            SlideService.InsertSlide(slide);
            return Ok("Insert success");
        }
        [HttpPut(nameof(UpdateSlide))]
        public IActionResult UpdateSlide(SlideDTO slideDTO)
        {
            var slide = _mapper.Map<Slide>(slideDTO);
            SlideService.UpdateSlide(slide);
            return Ok("Update success");
        }
        [HttpDelete(nameof(DeleteSlide))]
        public IActionResult DeleteSlide(int id)
        {
            SlideService.DeleteSlide(id);
            return Ok("Delete success");
        }
    }
}
