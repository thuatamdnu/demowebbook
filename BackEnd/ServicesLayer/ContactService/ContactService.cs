﻿using DomainLayer.Models;
using RepositoryLayer.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicesLayer.ContactService
{
    public class ContactService : IContactService
    {
        private IRepository<Contact> repo;
        public ContactService(IRepository<Contact> repo)
        {
            this.repo = repo;
        }
        public void DeleteContact(int id)
        {
            Contact contact = GetContact(id);
            repo.Remove(contact);
            repo.SaveChanges();
        }

        public IEnumerable<Contact> GetAllContact()
        {
            return repo.GetAll();
        }

        public Contact GetContact(int id)
        {
            return repo.Get(id);
        }

        public void InsertContact(Contact contact)
        {
            repo.Insert(contact);
        }

        public void UpdateContact(Contact contact)
        {
            repo.Update(contact);
        }
    }
}
