﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStoreDTO.DTO
{
    public class OrderDetailDTO
    {
        public int Id { get; set; }
        public bool IsActive { get; set; }
        public string Quantity { get; set; }
        public int BookId { get; set; }
        public int OrderId { get; set; }
    }
}
