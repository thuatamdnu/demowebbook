﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Query;
using RepositoryLayer.Repository.IRepo;
using RepositoryLayer;

namespace Supro.DAL.Repositories
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class, new()
    {
        private readonly BookStoreDbContext _suproDbContext;

        public GenericRepository(BookStoreDbContext suproDbContext)
        {
            _suproDbContext = suproDbContext;
        }

        public async Task<TEntity> Add(TEntity entity)
        {
            var addedEntry = await _suproDbContext.AddAsync(entity);
            await _suproDbContext.SaveChangesAsync();
            return entity;
        }

        public async Task<List<TEntity>> AddRange(List<TEntity> entity)
        {
            await _suproDbContext.AddRangeAsync(entity);
            await _suproDbContext.SaveChangesAsync();
            return entity;
        }

        public async Task<int> Delete(TEntity entity)
        {
            var deletedEntry = _suproDbContext.Remove(entity);
            return await _suproDbContext.SaveChangesAsync();
        }

        public async Task<TEntity> Get(Expression<Func<TEntity, bool>> filter)
        {
            return await _suproDbContext.Set<TEntity>().FirstOrDefaultAsync(filter);
        }

        public async Task<List<TEntity>> GetList(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            Func<IQueryable<TEntity>, IIncludableQueryable<TEntity, object>> include = null)
        {
            IQueryable<TEntity> query = _suproDbContext.Set<TEntity>();

            if (include != null)
            {
                query = include(query);
            }

            if (filter != null)
            {
                query = query.Where(filter);
            }

            if (orderBy != null)
            {
                query = orderBy(query);
            }

            return await query.ToListAsync();
        }
        
        public async Task<TEntity> Update(TEntity entity)
        {
            _suproDbContext.Update(entity);
            await _suproDbContext.SaveChangesAsync();
            return entity;
        }

        public async Task<List<TEntity>> UpdateRange(List<TEntity> entity)
        {
            _suproDbContext.UpdateRange(entity);
            await _suproDbContext.SaveChangesAsync();
            return entity;
        }
    }
}
