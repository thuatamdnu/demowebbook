﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStoreDTO.DTO
{
    public class OrderDTO
    {
        public int Id { get; set; }
        public int TotalMoney { get; set; }
        public string Payment { get; set; }
        public DateTime BookingDate { get; set; }
        public string DeliveryData { get; set; }
        public string ShippingWay { get; set; }
        public string State { get; set; }
        public string Note { get; set; }
        public string TransportFee { get; set; }
        public int CustomerId { get; set; }
    }
}
