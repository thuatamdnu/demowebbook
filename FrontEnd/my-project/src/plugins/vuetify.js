import Vue from 'vue'
import '@fortawesome/fontawesome-free/css/all.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import Carousel3d from 'vue-carousel-3d';
import 'vueperslides/dist/vueperslides.css'
import {VueperSlides, VueperSlide} from 'vueperslides';

Vue.use(Vuetify)
Vue.use(Carousel3d, VueperSlides, VueperSlide);

const opts = {}

export default new Vuetify({
    icons: {
        iconfont: 'md' || 'fa'
    }
});
