﻿using DomainLayer.Models;
using RepositoryLayer.Repository;
using RepositoryLayer.Repository.IRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicesLayer.BookService
{
    public class BookService : IBookService
    {
        private IRepository<Book> repo;
        public BookService(IRepository<Book> repo )
        {
           
            this.repo = repo;
        }
        public void DeleteBook(int id)
        {
            Book book = GetBook(id);
            repo.Remove(book);
            repo.SaveChanges();
        }

        public IEnumerable<Book> GetAllBook()
        {
            return repo.GetAll();
        }

     
      
        public void InsertBook(Book book)
        {
            repo.Insert(book);
        }

        public void UpdateBook(Book book)
        {
            repo.Update(book);
        }

        public Book GetBook(int id)
        {
             return repo.Get(id);
        }

        
    }
}
