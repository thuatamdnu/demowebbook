﻿using DomainLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.EntityMapper
{
    public class MenuTypeMap : IEntityTypeConfiguration<MenuType>
    {
        public void Configure(EntityTypeBuilder<MenuType> builder)
        {
            builder.HasKey(x => x.Id).HasName("pk_menutypeid");
            builder.Property(x => x.Id).ValueGeneratedOnAdd()
                .HasColumnName("Id").HasColumnType("INT");
            builder.Property(x => x.Position)
                .HasColumnType("NVARCHAR(50)").IsRequired();
            builder.Property(x => x.Description)
                .HasColumnType("NVARCHAR(50)").IsRequired();
        }
    }
}
