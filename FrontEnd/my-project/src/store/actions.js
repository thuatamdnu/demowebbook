import axios from "axios"
export const getBooks = ({commit}) =>{
    axios.get('https://localhost:44398/api/Book/GetAllBook')
    .then(response => {
        commit('SET_BOOKS' , response.data);
    })
}

export const getBook = ({commit},getbookId ) => {
    axios.get('https://localhost:44398/api/Book/GetBook?id='+getbookId)
    .then(response => {
        commit('SET_PRODUCT' , response.data);
    })
}

export const addBookToCart = ({commit}, {getbook , quantity}) =>{
    commit('ADD_TO_CART', {getbook,quantity});
}
export const removeBookFromCart = ({commit}, getbook) =>{
    commit('REMOVE_BOOK_FROM_CAT',getbook);
}