﻿using AutoMapper;
using BookStoreDTO.DTO;
using DomainLayer.Models;
using RepositoryLayer.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicesLayer.CategoryService
{
    public class CategoryService : ICategoryService
    {
        private readonly IMapper _mapper;
        private IRepository<Category> repo;
        public CategoryService(IRepository<Category> repo , IMapper mapper)
        {
            this.repo = repo;
            _mapper = mapper;
        }
        public Category GetCategory(int id)
        {
            return repo.Get(id);
        }
        public void DeleteCategory(int id)
        {
            Category customer = GetCategory(id);
            repo.Remove(customer);
            repo.SaveChanges();
        }

        public IEnumerable<Category> GetAllCategory()
        {

            return repo.GetAll();
        }

        public void InsertCategory(Category category)
        {
            repo.Insert(category);
        }

  
        public void UpdateCategory(Category category)
        {
            repo.Update(category);
        }

    }
    
}
