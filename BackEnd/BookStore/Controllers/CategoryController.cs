﻿using AutoMapper;
using BookStoreDTO.DTO;
using DomainLayer.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServicesLayer.CategoryService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eShopOnWeb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly ICategoryService categoryService;
        private readonly IMapper _mapper;
        //private readonly IWebHostEnvironment _webHostEnvironment;
       public CategoryController(ICategoryService categoryService , IMapper mapper)
        {
            this.categoryService = categoryService;
            _mapper = mapper;
            //_webHostEnvironment = webHostEnvironment;
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            var config = new MapperConfiguration(cfg => cfg.CreateMap<Category, CategoryDTO>().ReverseMap());
            _mapper = new Mapper(config);

        }
        [HttpGet(nameof(GetCategory))]
        public IActionResult GetCategory(int id)
        {
            var result = categoryService.GetCategory(id);
            if (result is not null)
            {
                return Ok(result);
            }
            return BadRequest("Not found");
        }
        [HttpGet(nameof(GetAllCategory))]
        public IActionResult GetAllCategory()
        {
          
            
            var category = categoryService.GetAllCategory();
            var categoryDTO = _mapper.Map<IEnumerable<CategoryDTO>>(category);
            if (categoryDTO is not null)
            {
                return Ok(categoryDTO);
            }
            return BadRequest("Not Found");
        }
        [HttpPost(nameof(InsertCategory))]
        public IActionResult InsertCategory(CategoryDTO categoryDTO)
        {
            var category = _mapper.Map<Category>(categoryDTO);
            categoryService.InsertCategory(category);
            return Ok("Insert success");
        }
        [HttpPut(nameof(UpdateCategory))]
        public IActionResult UpdateCategory(CategoryDTO categoryDTO)
        {
            var category = _mapper.Map<Category>(categoryDTO);
            categoryService.UpdateCategory(category);
            return Ok("Update success");
        }
        [HttpDelete(nameof(DeleteCategory))]
        public IActionResult DeleteCategory(int id)
        {
            categoryService.DeleteCategory(id);
            return Ok("Delete success");
        }
    }
}
