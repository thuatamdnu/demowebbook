﻿using AutoMapper;
using BookStoreDTO.DTO;
using BookStoreDTO.DTO.Book;
using DomainLayer.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using ServicesLayer.BookService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eShopOnWeb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : Controller
    {
        private readonly IBookService bookService;
        private readonly IMapper _mapper;
        private readonly IWebHostEnvironment _webHostEnvironment;
        public BookController(IBookService bookService , IMapper mapper  )
        {
            this.bookService = bookService;
            _mapper = mapper;
            //_webHostEnvironment = webHostEnvironment;
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            var config = new MapperConfiguration(cfg => cfg.CreateMap<Book, BookDTO>().ReverseMap());
            _mapper = new Mapper(config);
          
        }
        [HttpGet(nameof(GetBook))]
        public IActionResult GetBook(int id)
        {
            var result = bookService.GetBook(id);
            if (result is not null)
            {
                return Ok(result);
            }
            return BadRequest("Not found");
        }
        [HttpGet(nameof(GetAllBook))]
        public IActionResult GetAllBook()
        {
            var book = bookService.GetAllBook();
            var bookGetDTO = _mapper.Map<IEnumerable<BookDTO>>(book);

            if (bookGetDTO is not null)
            {
                return Ok(bookGetDTO);
            }
            return BadRequest("Not Found");
           
        }
        [HttpPost(nameof(InsertBook))]
        public IActionResult InsertBook(BookDTO bookDTO)
        {

            var book = _mapper.Map<Book>(bookDTO);
            bookService.InsertBook(book);
            return Ok("Insert success");
        }
        [HttpPut(nameof(UpdateBook))]
        public IActionResult UpdateBook(BookDTO bookDTO)
        {
            var book = _mapper.Map<Book>(bookDTO);
            bookService.UpdateBook(book);
            return Ok("Insert success");
        }
        [HttpDelete(nameof(DeleteBook))]
        public IActionResult DeleteBook(int id)
        {
            bookService.DeleteBook(id);
            return Ok("Delete success");
        }
    }
}
