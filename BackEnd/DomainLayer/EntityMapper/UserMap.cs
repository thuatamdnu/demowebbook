﻿using DomainLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.EntityMapper
{
    public class UserMap : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.HasKey(x => x.Id).HasName("pk_userid");
            builder.Property(x => x.Id).ValueGeneratedOnAdd()
                .HasColumnName("Id").HasColumnType("INT");
            builder.Property(x => x.UserName)
                .HasColumnType("NVARCHAR(50)").IsRequired();
            builder.Property(x => x.Password)
                .HasColumnType("NVARCHAR(50)").IsRequired();
            builder.Property(x => x.Fullname)
                .HasColumnType("NVARCHAR(50)").IsRequired();
            builder.Property(x => x.Avatar)
                .HasColumnType("NVARCHAR(50)").IsRequired();
            builder.Property(x => x.Email)
                .HasColumnType("NVARCHAR(50)").IsRequired();
        }
    }
}
