﻿using DomainLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.EntityMapper
{
    public class BookMap : IEntityTypeConfiguration<Book>
    {
        public void Configure(EntityTypeBuilder<Book> builder)
        {
            builder.HasKey(x => x.Id).HasName("pk_bookid");
            builder.Property(x => x.Id).UseIdentityColumn()
                .HasColumnName("BookId")
                .HasColumnType("INT");
            builder.Property(x => x.BookName)
            .HasColumnName("BookName")
            .HasColumnType("NVARCHAR(100)")
            .IsRequired();
        builder.HasOne(x => x.Category)
            .WithMany(x => x.Books)
            .HasForeignKey(x => x.CategoryId);
        builder.HasOne(x => x.Supplier)
            .WithMany(x => x.Books)
            .HasForeignKey(x => x.CompanyId);
    }
}
}
