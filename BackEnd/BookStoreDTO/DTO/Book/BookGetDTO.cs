﻿using DomainLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStoreDTO.DTO.Book
{
    public class BookGetDTO
    {
        public int ID { get; set; }
        public string BookName { get; set; }
        public float Price { get; set; }
        public float OriginalPrice { get; set; }
        public string Description { get; set; }
        public bool Discount { get; set; }

        public Category CategoryID { get; set; }
        public Category CategoryName { get; set; }
        public Supplier CompanyID { get; set; }


    }
}
