﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStoreDTO.DTO
{
    public class MenuTypeDTO
    {
        public int Id { get; set; }
        public string Position { get; set; }
        public string Description { get; set; }
    }
}
