﻿using AutoMapper;
using BookStoreDTO.DTO;
using DomainLayer.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using ServicesLayer.UserService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eShopOnWeb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
     public class UserController : Controller
    {
        private readonly IUserService UserService;
        private readonly IMapper _mapper;
        private readonly IWebHostEnvironment _webHostEnvironment;
        public UserController(IUserService UserService, IMapper mapper)
        {
            this.UserService = UserService;
            _mapper = mapper;
            //_webHostEnvironment = webHostEnvironment;
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            var config = new MapperConfiguration(cfg => cfg.CreateMap<User, UserDTO>().ReverseMap());
            _mapper = new Mapper(config);
        }
        //[HttpPost(nameof(Login))]
        //public IActionResult Login([FromBody] UserDTO userDTO)
        //{
        //    if (userDTO.UserName == "admin" && userDTO.Password == "pass")
        //    {

        //        return BadRequest("đăng nhập thành công" ) ;
                      
        //    }
        //    else
        //    {
        //        return BadRequest("Đăng nhập thất bại");    
        //    }
        //    //var result = UserService.GetUser(id);
        //    //if (result is not null)
        //    //{
        //    //    return Ok(result);
        //    //}
        //    //return BadRequest("Not found");
        //}
        [HttpPost(nameof(Login))]
        public ActionResult Login([FromBody] UserDTO userDTO)
        {
            if (userDTO.UserName == "admin" && userDTO.Password == "pass")
            {
                return Json( new { success = true });
            }
            else
            {
                return Json(new { success = false });
            }
        }

        [HttpGet(nameof(GetUser))]
        public IActionResult GetUser(int id)
        {
            var result = UserService.GetUser(id);
            if (result is not null)
            {
                return Ok(result);
            }
            return BadRequest("Not found");
        }
        [HttpGet(nameof(GetAllUser))]
        public IActionResult GetAllUser()
        {
            var result = UserService.GetAllUser();
            var userDTO = _mapper.Map<IEnumerable<UserDTO>>(result);
            if (userDTO is not null)
            {
                return Ok(userDTO);
            }
            return BadRequest("Not Found");
        }
        [HttpPost(nameof(InsertUser))]
        public IActionResult InsertUser(UserDTO userDTO)
        {
            var user = _mapper.Map<User>(userDTO);
            UserService.InsertUser(user);
            return Ok("Insert success");
        }
        [HttpPut(nameof(UpdateUser))]
        public IActionResult UpdateUser(UserDTO userDTO)
        {
            var user = _mapper.Map<User>(userDTO);
            UserService.UpdateUser(user);
            return Ok("Update success");
        }
        [HttpDelete(nameof(DeleteUser))]
        public IActionResult DeleteUser(int id)
        {
            UserService.DeleteUser(id);
            return Ok("Delete success");
        }
    }
}
