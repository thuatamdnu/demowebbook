﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStoreDTO.DTO
{
    public class MenuDTO
    {
        public int Id { get; set; }
        public string MenuName { get; set; }
        public string Url { get; set; }
        public int SortOrder { get; set; }
        public int MenuTypeId { get; set; }
      
    }
}
