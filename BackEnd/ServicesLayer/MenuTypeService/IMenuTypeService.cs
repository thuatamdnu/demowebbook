﻿using DomainLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicesLayer.MenuTypeService
{
    public interface IMenuTypeService
    {
        IEnumerable<MenuType> GetAllMenuType();
        MenuType GetMenuType(int id);
        void InsertMenuType(MenuType menuType);
        void UpdateMenuType(MenuType menuType);
        void DeleteMenuType(int id);
    }
}
