﻿using DomainLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicesLayer.BookImageService
{
    public interface IBookImageService
    {
        IEnumerable<BookImage> GetAllBookImage();
        BookImage GetBookImage(int id);
        void InsertBookImage(BookImage bookImage);
        void UpdateBookImage(BookImage bookImage);
        void DeleteBookImage(int id);
    }
}

