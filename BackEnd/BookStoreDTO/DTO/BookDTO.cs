﻿using DomainLayer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStoreDTO.DTO
{
    public class BookDTO
    {
        public int ID { get; set; }
        public string BookName { get; set; }
        public float Price { get; set; }
        public float OriginalPrice { get; set; }
        public string Description { get; set; }
        public bool Discount { get; set; }
        
        public int CategoryID { get; set; }
        [ForeignKey("CategoryID")]
        public virtual Category Category { get; set; }

        public int CompanyID { get; set; }
      
    }
}
