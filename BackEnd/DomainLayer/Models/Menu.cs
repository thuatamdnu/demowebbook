﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.Models
{
    public class Menu : BaseEntity
    {
        public string MenuName { get; set; }
        public string Url { get; set; }
        public int SortOrder { get; set; }
        public int MenuTypeId { get; set; }
        public virtual MenuType MenuType { get; set; }
    }
}
