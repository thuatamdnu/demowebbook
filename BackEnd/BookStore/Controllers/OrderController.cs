﻿using AutoMapper;
using BookStoreDTO.DTO;
using DomainLayer.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using ServicesLayer.OrderService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eShopOnWeb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService orderService;
        private readonly IMapper _mapper;
        private readonly IWebHostEnvironment _webHostEnvironment;
        public OrderController(IOrderService orderService, IMapper mapper)
        {
            this.orderService = orderService;
            _mapper = mapper;
            //_webHostEnvironment = webHostEnvironment;
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            var config = new MapperConfiguration(cfg => cfg.CreateMap<Order, OrderDTO>().ReverseMap());
            _mapper = new Mapper(config);
        }
        [HttpGet(nameof(GetOrder))]
        public IActionResult GetOrder(int id)
        {
            var result = orderService.GetOrder(id);
            if (result is not null)
            {
                return Ok(result);
            }
            return BadRequest("Not found");
        }
        [HttpGet(nameof(GetAllOrder))]
        public IActionResult GetAllOrder()
        {
            var result = orderService.GetAllOrder();
            var orderDTO = _mapper.Map<IEnumerable<OrderDTO>>(result);
            if (orderDTO is not null)
            {
                return Ok(orderDTO);
            }
            return BadRequest("Not Found");
        }
        [HttpPost(nameof(InsertOrder))]
        public IActionResult InsertOrder(OrderDTO orderDTO)
        {
            var order = _mapper.Map<Order>(orderDTO);
            orderService.InsertOrder(order);
            return Ok("Insert success");
        }
        [HttpPut(nameof(UpdateOrder))]
        public IActionResult UpdateOrder(OrderDTO orderDTO)
        {
            var order = _mapper.Map<Order>(orderDTO);
            orderService.UpdateOrder(order);
            return Ok("Update success");
        }
        [HttpDelete(nameof(DeleteOrder))]
        public IActionResult DeleteOrder(int id)
        {
            orderService.DeleteOrder(id);
            return Ok("Delete success");
        }
    }
}
