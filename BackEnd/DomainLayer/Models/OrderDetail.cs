﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.Models
{
    public class OrderDetail : BaseEntity
    {
        
        public string Quantity { get; set; }
        public virtual Order Order { get; set; }
        public virtual Book Book { get; set; }
        public int BookId { get; set; }
        public int OrderId { get; set; }
    }
}
