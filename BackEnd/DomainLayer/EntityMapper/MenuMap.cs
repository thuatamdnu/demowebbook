﻿using DomainLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.EntityMapper
{
    public class MenuMap : IEntityTypeConfiguration<Menu>
    {
        public void Configure(EntityTypeBuilder<Menu> builder)
        {
            builder.HasKey(x => x.Id).HasName("pk_menuid");
            builder.Property(x => x.Id).ValueGeneratedOnAdd()
                .HasColumnName("Id").HasColumnType("INT");
            builder.Property(x => x.MenuName)
                .HasColumnType("NVARCHAR(50)").IsRequired();
            builder.Property(x => x.Url)
                .HasColumnType("NVARCHAR(50)").IsRequired();
            builder.Property(x => x.SortOrder)
                .HasColumnType("NVARCHAR(50)").IsRequired();

            builder.HasOne(x => x.MenuType)
                .WithMany(x => x.Menus)
                .HasForeignKey(x => x.MenuTypeId);
        }
    }
}
