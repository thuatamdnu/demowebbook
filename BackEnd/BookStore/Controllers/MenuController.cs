﻿using AutoMapper;
using BookStoreDTO.DTO;
using DomainLayer.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using ServicesLayer.MenuService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eShopOnWeb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MenuController : ControllerBase
    {
        private readonly IMenuService menuService;
        private readonly IMapper _mapper;
        private readonly IWebHostEnvironment _webHostEnvironment;
        public MenuController(IMenuService menuService,IMapper mapper)
        {
            this.menuService = menuService;
            _mapper = mapper;
            //_webHostEnvironment = webHostEnvironment;
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            var config = new MapperConfiguration(cfg => cfg.CreateMap<Menu, MenuDTO>().ReverseMap());
            _mapper = new Mapper(config);
        }
        [HttpGet(nameof(GetMenu))]
        public IActionResult GetMenu(int id)
        {
            var result = menuService.GetMenu(id);
            if (result is not null)
            {
                return Ok(result);
            }
            return BadRequest("Not found");
        }
        [HttpGet(nameof(GetAllMenu))]
        public IActionResult GetAllMenu()
        {
            var result = menuService.GetAllMenu();
            var menuDTO = _mapper.Map<IEnumerable<MenuDTO>>(result);
            if (menuDTO is not null)
            {
                return Ok(menuDTO);
            }
            return BadRequest("Not Found");
        }
        [HttpPost(nameof(InsertMenu))]
        public IActionResult InsertMenu(MenuDTO menuDTO)
        {
            var menu = _mapper.Map<Menu>(menuDTO);
            menuService.InsertMenu(menu);
            return Ok("Insert success");
        }
        [HttpPut(nameof(UpdateMenu))]
        public IActionResult UpdateMenu(MenuDTO menuDTO)
        {
            var menu = _mapper.Map<Menu>(menuDTO);
            menuService.UpdateMenu(menu);
            return Ok("Update success");
        }
        [HttpDelete(nameof(DeleteMenu))]
        public IActionResult DeleteMenu(int id)
        {
            menuService.DeleteMenu(id);
            return Ok("Delete success");
        }
    }
}
