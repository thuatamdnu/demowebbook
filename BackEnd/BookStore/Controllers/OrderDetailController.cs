﻿using AutoMapper;
using BookStoreDTO.DTO;
using DomainLayer.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using ServicesLayer.OrderDetailService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eShopOnWeb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderDetailController : ControllerBase
    {
        private readonly IOrderDetailService orderDetailService;
        private readonly IMapper _mapper;
        private readonly IWebHostEnvironment _webHostEnvironment;
        public OrderDetailController(IOrderDetailService orderDetailService, IMapper mapper)
        {
            this.orderDetailService = orderDetailService;
            _mapper = mapper;
            //_webHostEnvironment = webHostEnvironment;
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            var config = new MapperConfiguration(cfg => cfg.CreateMap<OrderDetail, OrderDetailDTO>().ReverseMap());
            _mapper = new Mapper(config);
        }
        [HttpGet(nameof(GetOrderDetail))]
        public IActionResult GetOrderDetail(int id)
        {
            var result = orderDetailService.GetOrderDetail(id);
            if (result is not null)
            {
                return Ok(result);
            }
            return BadRequest("Not found");
        }
        [HttpGet(nameof(GetAllOrderDetail))]
        public IActionResult GetAllOrderDetail()
        {
            var result = orderDetailService.GetAllOrderDetail();
            var orderDetailDTO = _mapper.Map<IEnumerable<OrderDetailDTO>>(result);
            if (orderDetailDTO is not null)
            {
                return Ok(orderDetailDTO);
            }
            return BadRequest("Not Found");
        }
        [HttpPost(nameof(InsertOrderDetail))]
        public IActionResult InsertOrderDetail(OrderDetailDTO orderDetailDTO)
        {
            var orderDetail = _mapper.Map<OrderDetail>(orderDetailDTO);
            orderDetailService.InsertOrderDetail(orderDetail);
            return Ok("Insert success");
        }
        [HttpPut(nameof(UpdateOrderDetail))]
        public IActionResult UpdateOrderDetail(OrderDetailDTO orderDetailDTO)
        {
            var orderDetail = _mapper.Map<OrderDetail>(orderDetailDTO);
            orderDetailService.UpdateOrderDetail(orderDetail);
            return Ok("Update success");
        }
        [HttpDelete(nameof(DeleteOrderDetail))]
        public IActionResult DeleteOrderDetail(int id)
        {
            orderDetailService.DeleteOrderDetail(id);
            return Ok("Delete success");
        }
    }
}
