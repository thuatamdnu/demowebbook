﻿using DomainLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.EntityMapper
{
    public class OrderDetailMap : IEntityTypeConfiguration<OrderDetail>
    {
        public void Configure(EntityTypeBuilder<OrderDetail> builder)
        {
            builder.HasKey(x => x.Id).HasName("pk_orderdetailid");
            builder.Property(x => x.Id).ValueGeneratedOnAdd()
                .HasColumnName("Id").HasColumnType("INT");
            builder.Property(x => x.Quantity)
                .HasColumnType("NVARCHAR(50)").IsRequired();

            builder.HasOne(x => x.Book)
                .WithMany(x => x.OrderDetails)
                .HasForeignKey(x => x.BookId);
            builder.HasOne(x => x.Order)
                .WithMany(x => x.OrderDetails)
                .HasForeignKey(x => x.OrderId);
        }
    }
}
