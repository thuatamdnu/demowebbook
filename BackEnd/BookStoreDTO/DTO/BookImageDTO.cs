﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStoreDTO.DTO
{
    public class BookImageDTO
    {
        public int Id { get; set; }
        public string ImagePath { get; set; }
        public bool IsDefault { get; set; }
        public int BookId { get; set; }
    }
}
