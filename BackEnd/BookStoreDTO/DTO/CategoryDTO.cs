﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BookStoreDTO.DTO
{
    public class CategoryDTO
    {
        
        [Key]
        public int ID { get; set; }
        public string CategoryName { get; set; }
        public string Description { get; set; }
     
    }
}
