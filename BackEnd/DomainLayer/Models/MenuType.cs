﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.Models
{
    public class MenuType : BaseEntity
    {
        public string Position { get; set; }
        public string Description { get; set; }
        public virtual List<Menu> Menus { get; set; }
    }
}
