﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.Models
{
    public class Book:BaseEntity
    {
        public string BookName { get; set; }
        public float Price { get; set; }
        public float OriginalPrice { get; set; }
        public string Description { get; set; }
        public bool Discount { get; set; }
        [Required]
        public int CategoryId { get; set; }
        [Required]
        public int CompanyId { get; set; }
        public virtual List<BookImage> BookImages { get; set; }
        public virtual List<OrderDetail> OrderDetails { get; set; }
        public virtual Category Category { get; set; }
        public virtual Supplier Supplier { get; set; }
    }
}
