import Vue from 'vue'
import Router from 'vue-router'
import MainLayout from '@/layouts/MainLayout'
import ProductDetail from '@/views/Product/ProductDetail'
import MiniCart from '@/views/Product/MiniCart'
import BookIndex from '@/layouts/book/BookIndex'
import CategoryIndex from '@/layouts/category/CategoryIndex'
import BookImageIndex from '@/layouts/bookimage/BookImageIndex'
import ContactIndex from '@/layouts/contact/ContactIndex'
import CustomerIndex from '@/layouts/customer/CustomerIndex'
import MenuIndex from '@/layouts/menu/MenuIndex'
import MenuTypeIndex from '@/layouts/menutype/MenuTypeIndex'
import OrderIndex from '@/layouts/order/OrderIndex'
import OrderDetailIndex from '@/layouts/orderdetail/OrderDetailIndex'
import SlideIndex from '@/layouts/slide/SlideIndex'
import SupplierIndex from '@/layouts/supplier/SupplierIndex'
import UserIndex from '@/layouts/user/UserIndex'
import Login from '@/views/Product/Login'
import Orderdetail from '@/views/Product/OrderDetails'
import Tintuc from '@/views/Product/tintuc'
import dataStore from "../store";
import axios from 'axios'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'MainLayout',
      component: MainLayout
    },
    {
      path: '/tintuc',
      name: 'Tintuc',
      component: Tintuc
    },
    {
      path: '/Orderdetail',
      name: 'OrderDetails',
      component: Orderdetail
    },
    {
      path: '/productdetail/:id',
      name: 'ProductDetail',
      component: ProductDetail,
      props: true
    },
    {
      path: '/minicart',
      name: 'MiniCart',
      component: MiniCart,
      props: true
    },
    {
      path: '/login',
      name: 'Login',
      component: Login,
      
    },
    {
      path: '/book',
      name: 'BookIndex',
      component: BookIndex,
      beforeEnter(to, from, next) {
        if (dataStore.state.auth.authenticated) {
            next();
        } else {
            alert("bạn cần đăng nhập với quền admin")
            next("/Login");
        }
      }
    },
    {
      path: '/category',
      name: 'CategoryIndex',
      component: CategoryIndex,
      beforeEnter(to, from, next) {
        if (dataStore.state.auth.authenticated) {
            next();
        } else {
            alert("bạn cần đăng nhập với quền admin")
            next("/Login");
        }
      }
    },
    {
      path: '/bookimage',
      name: 'BookImageIndex',
      component: BookImageIndex,
      beforeEnter(to, from, next) {
        if (dataStore.state.auth.authenticated) {
            next();
        } else {
            alert("bạn cần đăng nhập với quền admin")
            next("/Login");
        }
      }
    },
    {
      path: '/contact',
      name: 'ContactIndex',
      component: ContactIndex,
      beforeEnter(to, from, next) {
        if (dataStore.state.auth.authenticated) {
            next();
        } else {
            alert("bạn cần đăng nhập với quền admin")
            next("/Login");
        }
      }
    },
    {
      path: '/customer',
      name: 'CustomerIndex',
      component: CustomerIndex,
      beforeEnter(to, from, next) {
        if (dataStore.state.auth.authenticated) {
            next();
        } else {
            alert("bạn cần đăng nhập với quền admin")
            next("/Login");
        }
      }
    },
    {
      path: '/menu',
      name: 'MenuIndex',
      component: MenuIndex,
      beforeEnter(to, from, next) {
        if (dataStore.state.auth.authenticated) {
            next();
        } else {
            alert("bạn cần đăng nhập với quền admin")
            next("/Login");
        }
      }
    },
    {
      path: '/menutype',
      name: 'MenuTypeIndex',
      component: MenuTypeIndex,
      beforeEnter(to, from, next) {
        if (dataStore.state.auth.authenticated) {
            next();
        } else {
            alert("bạn cần đăng nhập với quền admin")
            next("/Login");
        }
      }
    },
    {
      path: '/order',
      name: 'OrderIndex',
      component: OrderIndex,beforeEnter(to, from, next) {
        if (dataStore.state.auth.authenticated) {
            next();
        } else {
            alert("bạn cần đăng nhập với quền admin")
            next("/Login");
        }
      }

    },
    {
      path: '/orderdetail',
      name: 'OrderDetailIndex',
      component: OrderDetailIndex,
      beforeEnter(to, from, next) {
        if (dataStore.state.auth.authenticated) {
            next();
        } else {
            alert("bạn cần đăng nhập với quền admin")
            next("/Login");
        }
      }
    },
    {
      path: '/slide',
      name: 'SlideIndex',
      component: SlideIndex,
      beforeEnter(to, from, next) {
        if (dataStore.state.auth.authenticated) {
            next();
        } else {
            alert("bạn cần đăng nhập với quền admin")
            next("/Login");
        }
      }
    },
    {
      path: '/supplier',
      name: 'SupplierIndex',
      component: SupplierIndex,
      beforeEnter(to, from, next) {
        if (dataStore.state.auth.authenticated) {
            next();
        } else {
            alert("bạn cần đăng nhập với quền admin")
            next("/Login");
        }
      }
    },
    {
      path: '/user',
      name: 'UserIndex',
      component: UserIndex,
      beforeEnter(to, from, next) {
        if (dataStore.state.auth.authenticated) {
            next();
        } else {
            alert("bạn cần đăng nhập với quền admin")
            next("/Login");
        }
      }
    },
  ]
})


