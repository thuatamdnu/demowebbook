﻿using DomainLayer.Models;
using Microsoft.EntityFrameworkCore.Query;
using RepositoryLayer.Repository.IRepo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace RepositoryLayer.Repository
{
    public class BookRepo : IBookRepo
    {
        private readonly BookStoreDbContext _suproDbContext;
        public object GetBook()
        {
            object book = (from b in _suproDbContext.Book 
                              join cate in _suproDbContext.Category on b.CategoryId equals cate.Id
                              join sup in _suproDbContext.Supplier on b.CompanyId equals sup.Id
                              select new
                              {
                                  b.Id,
                                  b.Price,
                                  b.BookName,
                                  b.CategoryId,
                                  b.CompanyId,
                                  b.Discount,
                                  cate.CategoryName,
                                  cate.Description,
                                  sup.CompanyName,
                              }).ToList();
            return book;
        }

        public Task<List<Book>> GetList(Expression<Func<Book, bool>> filter = null, Func<IQueryable<Book>, IOrderedQueryable<Book>> orderBy = null, Func<IQueryable<Book>, IIncludableQueryable<Book, object>> include = null)
        {
            throw new NotImplementedException();
        }
    }
}
