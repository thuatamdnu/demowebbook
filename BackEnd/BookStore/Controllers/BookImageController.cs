﻿using AutoMapper;
using BookStoreDTO.DTO;
using DomainLayer.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using ServicesLayer.BookImageService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eShopOnWeb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BookImageController : ControllerBase
    {
        private readonly IBookImageService bookImageService;
        private readonly IMapper _mapper;
        private readonly IWebHostEnvironment _webHostEnvironment;
        public BookImageController(IBookImageService bookImageService, IMapper mapper)
        {
            this.bookImageService = bookImageService;
           
            _mapper = mapper;
            //_webHostEnvironment = webHostEnvironment;
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            var config = new MapperConfiguration(cfg => cfg.CreateMap<BookImage, BookImageDTO>().ReverseMap());
            _mapper = new Mapper(config);
        }
        [HttpGet(nameof(GetBookImage))]
        public IActionResult GetBookImage(int id)
        {
            var result = bookImageService.GetBookImage(id);
            if (result is not null)
            {
                return Ok(result);
            }
            return BadRequest("Not found");
        }
        [HttpGet(nameof(GetAllBookImage))]
        public IActionResult GetAllBookImage()
        {
            var result = bookImageService.GetAllBookImage();
            var bookGetDTO = _mapper.Map<IEnumerable<BookImageDTO>>(result);
            if (bookGetDTO is not null)
            {
                return Ok(bookGetDTO);
            }
            return BadRequest("Not Found");
        }
        [HttpPost(nameof(InsertBookImage))]
        public IActionResult InsertBookImage(BookImageDTO bookImageDTO)
        {
            var book = _mapper.Map<BookImage>(bookImageDTO);
            bookImageService.InsertBookImage(book);
            return Ok("Insert success");
        }
        [HttpPut(nameof(UpdateBookImage))]
        public IActionResult UpdateBookImage(BookImageDTO bookImageDTO)
        {
            var book = _mapper.Map<BookImage>(bookImageDTO);
            bookImageService.UpdateBookImage(book);
            return Ok("Update success");
        }
        [HttpDelete(nameof(DeleteBookImage))]
        public IActionResult DeleteBookImage(int id)
        {
            bookImageService.DeleteBookImage(id);
            return Ok("Delete success");
        }
    }
}
