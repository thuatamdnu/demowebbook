﻿using DomainLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RepositoryLayer.Repository.IRepo
{
    public interface IBookRepo : IGenericRepository<Book>
    {
        object GetBook();
    }
}
