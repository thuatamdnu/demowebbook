﻿using DomainLayer.Models;
using RepositoryLayer.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicesLayer.MenuTypeService
{
    public class MenuTypeService : IMenuTypeService
    {
        private IRepository<MenuType> repo;
        public MenuTypeService(IRepository<MenuType> repo)
        {
            this.repo = repo;
        }
        public void DeleteMenuType(int id)
        {
            MenuType menuType = GetMenuType(id);
            repo.Remove(menuType);
            repo.SaveChanges();
        }

        public IEnumerable<MenuType> GetAllMenuType()
        {
            return repo.GetAll();
        }

        public MenuType GetMenuType(int id)
        {
            return repo.Get(id);
        }

        public void InsertMenuType(MenuType menuType)
        {
            repo.Insert(menuType);
        }

        public void UpdateMenuType(MenuType menuType)
        {
            repo.Update(menuType);
        }
    }
}
