﻿using DomainLayer.Models;
using RepositoryLayer.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicesLayer.OrderDetailService
{
    public class OrderDetailService : IOrderDetailService
    {
        private IRepository<OrderDetail> repo;
        public OrderDetailService(IRepository<OrderDetail> repo)
        {
            this.repo = repo;
        }
        public void DeleteOrderDetail(int id)
        {
            OrderDetail orderDetail = GetOrderDetail(id);
            repo.Remove(orderDetail);
            repo.SaveChanges();
        }

        public IEnumerable<OrderDetail> GetAllOrderDetail()
        {
            return repo.GetAll();
        }

        public OrderDetail GetOrderDetail(int id)
        {
            return repo.Get(id);
        }

        public void InsertOrderDetail(OrderDetail orderDetail)
        {
            repo.Insert(orderDetail);
        }

        public void UpdateOrderDetail(OrderDetail orderDetail)
        {
            repo.Update(orderDetail);
        }
    }
}
