﻿using AutoMapper;
using DomainLayer.Models;
using BookStoreDTO.DTO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using ServicesLayer.CustomerService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BookStore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : Controller
    {
        private readonly ICustomerService customerService;
        private readonly IMapper _mapper;
        private readonly IWebHostEnvironment _webHostEnvironment;
        public CustomerController(ICustomerService customerService, IMapper mapper)
        {
            this.customerService = customerService;
            _mapper = mapper;
            //_webHostEnvironment = webHostEnvironment;
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            var config = new MapperConfiguration(cfg => cfg.CreateMap<Customer, CustomerDTO>().ReverseMap());
            _mapper = new Mapper(config);
        }

        [HttpPost(nameof(Login))]
        public ActionResult Login([FromBody] UserDTO userDTO)
        {
            if (userDTO.UserName == "admin" && userDTO.Password == "pass" && userDTO.Id == 3)
            {
                return Json(new { success = true });
            }
            else
            {
                return Json(new { success = false });
            }
        }

        [HttpGet(nameof(GetCustomer))]
        public IActionResult GetCustomer(int id , UserDTO userDTO)
        {
            
                var result = customerService.GetCustomer(id);
            
                if (result is not null)
                {
                    return Ok(result);
                }
                return BadRequest("Not found");
               
            
        }
        [HttpGet(nameof(GetAllCustomer))]
        public IActionResult GetAllCustomer()
        {
            var customer = customerService.GetAllCustomer();
            var customerDTO = _mapper.Map<IEnumerable<CustomerDTO>>(customer);

            if (customerDTO is not null)
            {
                return Ok(customerDTO);
            }
            return BadRequest("Not Found");
        }
        [HttpPost(nameof(InsertCustomer))]
        public IActionResult InsertCustomer(CustomerDTO customerDTO)
        {
            var customer = _mapper.Map<Customer>(customerDTO);
            customerService.InsertCustomer(customer);
            return Ok("Insert success");
        }
        [HttpPut(nameof(UpdateCustomer))]
        public IActionResult UpdateCustomer(CustomerDTO customerDTO)
        {
            var customer = _mapper.Map<Customer>(customerDTO);
            customerService.UpdateCustomer(customer);
            return Ok("Update success");
        }
        [HttpDelete(nameof(DeleteCustomer))]
        public IActionResult DeleteCustomer(int id)
        {
            customerService.DeleteCustomer(id);
            return Ok("Delete success");
        }
    }
}