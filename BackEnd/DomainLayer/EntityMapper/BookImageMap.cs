﻿using DomainLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.EntityMapper
{
    public class BookImageMap : IEntityTypeConfiguration<BookImage>
    {
        public void Configure(EntityTypeBuilder<BookImage> builder)
        {
            builder.HasKey(x => x.Id).HasName("pk_imageid");
            builder.Property(x => x.Id).UseIdentityColumn()
                .HasColumnName("Id")
                .HasColumnType("INT");

            builder.HasOne(x => x.Book)
                .WithMany(x => x.BookImages)
                .HasForeignKey(x => x.BookId);
        }
    }
}
