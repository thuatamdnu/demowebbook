﻿using DomainLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.EntityMapper
{
    public class SupplierMap : IEntityTypeConfiguration<Supplier>
    {
        public void Configure(EntityTypeBuilder<Supplier> builder)
        {
            builder.HasKey(x => x.Id).HasName("pk_companyid");
            builder.Property(x => x.Id).ValueGeneratedOnAdd()
                    .HasColumnName("Id").HasColumnType("INT");
            builder.Property(x => x.CompanyName)
                    .HasColumnType("NVARCHAR(50)").IsRequired();
            builder.Property(x => x.Address)
                    .HasColumnType("NVARCHAR(50)").IsRequired();
            builder.Property(x => x.Email)
                    .HasColumnType("NVARCHAR(50)").IsRequired();
            builder.Property(x => x.Phone)
                    .HasColumnType("NVARCHAR(50)").IsRequired();
        }
    }
}
