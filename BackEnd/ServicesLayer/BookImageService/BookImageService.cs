﻿using DomainLayer.Models;
using RepositoryLayer.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ServicesLayer.BookImageService
{
    public class BookImageService : IBookImageService
    {
        private IRepository<BookImage> repo;
        public BookImageService(IRepository<BookImage> repo)
        {
            this.repo = repo;
        }
        public void DeleteBookImage(int id)
        {
            BookImage bookImage = GetBookImage(id);
            repo.Remove(bookImage);
            repo.SaveChanges();
        }

        public IEnumerable<BookImage> GetAllBookImage()
        {
            return repo.GetAll();
        }

        public BookImage GetBookImage(int id)
        {
            return repo.Get(id);
        }

        public void InsertBookImage(BookImage bookImage)
        {
            repo.Insert(bookImage);
        }

        public void UpdateBookImage(BookImage bookImage)
        {
            repo.Update(bookImage);
        }
    }
}
