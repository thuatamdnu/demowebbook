import { getBook } from "./actions";

export const SET_BOOKS = (state , getallbooks) =>{
    state.getallbooks = getallbooks
}

export const SET_PRODUCT = (state, getbook) => {
    state.getbook = getbook;

}

export const ADD_TO_CART = (state, {getbook , quantity}) => {
    state.cart.push({
        getbook,
        quantity
    })
}

export const REMOVE_BOOK_FROM_CAT = (state , getbook) => {
    state.cart = state.cart.filter(item => {
        return item.getbook.id !== getbook.id;
    })
}