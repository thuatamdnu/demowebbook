﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.Models
{
    public class BookImage : BaseEntity
    {
        public string ImagePath { get; set; }
        public bool IsDefault { get; set; }
        public int BookId { get; set; }
        public virtual Book Book { get; set; }
    }
}
