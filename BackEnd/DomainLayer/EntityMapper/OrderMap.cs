﻿using DomainLayer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainLayer.EntityMapper
{
    public class OrderMap : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.HasKey(x => x.Id).HasName("pk_orderid");
            builder.Property(x => x.Id).ValueGeneratedOnAdd()
                .HasColumnName("Id").HasColumnType("INT");
            builder.Property(x => x.TotalMoney)
                .HasColumnType("NVARCHAR(50)").IsRequired();
            builder.Property(x => x.Payment)
                .HasColumnType("NVARCHAR(50)").IsRequired();
            builder.Property(x => x.BookingDate)
                .HasColumnType("NVARCHAR(50)").IsRequired();
            builder.Property(x => x.DeliveryData)
                .HasColumnType("NVARCHAR(50)").IsRequired();
            builder.Property(x => x.ShippingWay)
                .HasColumnType("NVARCHAR(50)").IsRequired();
            builder.Property(x => x.State)
                .HasColumnType("NVARCHAR(50)").IsRequired();
            builder.Property(x => x.Note)
                .HasColumnType("NVARCHAR(50)").IsRequired();
            builder.Property(x => x.TransportFee)
                .HasColumnType("NVARCHAR(50)").IsRequired();
            builder.HasOne(x => x.Customer)
                .WithMany(x => x.Orders)
                .HasForeignKey(x => x.CustomerId);
        }
    }
}
