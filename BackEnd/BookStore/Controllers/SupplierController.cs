﻿using AutoMapper;
using BookStoreDTO.DTO;
using DomainLayer.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ServicesLayer.SupplierService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eShopOnWeb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SupplierController : ControllerBase
    {
        private readonly ISupplierService SupplierService;
        private readonly IMapper _mapper;
        private readonly IWebHostEnvironment _webHostEnvironment;
        public SupplierController(ISupplierService SupplierService, IMapper mapper)
        {
            this.SupplierService = SupplierService;
            _mapper = mapper;
            //_webHostEnvironment = webHostEnvironment;
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            var config = new MapperConfiguration(cfg => cfg.CreateMap<Supplier, SupplierDTO>().ReverseMap());
            _mapper = new Mapper(config);
        }
        [HttpGet(nameof(GetSupplier))]
        public IActionResult GetSupplier(int id)
        {
            var result = SupplierService.GetSupplier(id);
            if (result is not null)
            {
                return Ok(result);
            }
            return BadRequest("Not found");
        }
        [HttpGet(nameof(GetAllSupplier))]
        public IActionResult GetAllSupplier()
        {
            var result = SupplierService.GetAllSupplier();
            var SupplierDTO = _mapper.Map<IEnumerable<SupplierDTO>>(result);
            if (SupplierDTO is not null)
            {
                return Ok(SupplierDTO);
            }
            return BadRequest("Not Found");
        }
        [HttpPost(nameof(InsertSupplier))]
        public IActionResult InsertSupplier(SupplierDTO supplierDTO)
        {
            var supplier = _mapper.Map<Supplier>(supplierDTO);
            SupplierService.InsertSupplier(supplier);
            return Ok("Insert success");
        }
        [HttpPut(nameof(UpdateSupplier))]
        public IActionResult UpdateSupplier(SupplierDTO supplierDTO)
        {
            var supplier = _mapper.Map<Supplier>(supplierDTO);
            SupplierService.UpdateSupplier(supplier);
            return Ok("Update success");
        }
        [HttpDelete(nameof(DeleteSupplier))]
        public IActionResult DeleteSupplier(int id)
        {
            SupplierService.DeleteSupplier(id);
            return Ok("Delete success");
        }
    }
}
