﻿using AutoMapper;
using BookStoreDTO.DTO;
using DomainLayer.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using ServicesLayer.MenuTypeService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eShopOnWeb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MenuTypeController : ControllerBase
    {
        private readonly IMenuTypeService menuTypeService;
        private readonly IMapper _mapper;
        private readonly IWebHostEnvironment _webHostEnvironment;
        public MenuTypeController(IMenuTypeService menuTypeService, IMapper mapper)
        {
            this.menuTypeService = menuTypeService;
            _mapper = mapper;
            //_webHostEnvironment = webHostEnvironment;
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            var config = new MapperConfiguration(cfg => cfg.CreateMap<MenuType, MenuTypeDTO>().ReverseMap());
            _mapper = new Mapper(config);
        }
        [HttpGet(nameof(GetMenuType))]
        public IActionResult GetMenuType(int id)
        {
            var result = menuTypeService.GetMenuType(id);
            if (result is not null)
            {
                return Ok(result);
            }
            return BadRequest("Not found");
        }
        [HttpGet(nameof(GetAllMenuType))]
        public IActionResult GetAllMenuType()
        {
            var result = menuTypeService.GetAllMenuType();
            var menutypeDTO = _mapper.Map<IEnumerable<MenuTypeDTO>>(result);
            if (menutypeDTO is not null)
            {
                return Ok(menutypeDTO);
            }
            return BadRequest("Not Found");
        }
        [HttpPost(nameof(InsertMenuType))]
        public IActionResult InsertMenuType(MenuTypeDTO menuTypeDTO)
        {
            var menutype = _mapper.Map<MenuType>(menuTypeDTO);
            menuTypeService.InsertMenuType(menutype);
            return Ok("Insert success");
        }
        [HttpPut(nameof(UpdateMenuType))]
        public IActionResult UpdateMenuType(MenuTypeDTO menuTypeDTO)
        {
            var menutype = _mapper.Map<MenuType>(menuTypeDTO);
            menuTypeService.UpdateMenuType(menutype);
            return Ok("Update success");
        }
        [HttpDelete(nameof(DeleteMenuType))]
        public IActionResult DeleteMenuType(int id)
        {
            menuTypeService.DeleteMenuType(id);
            return Ok("Delete success");
        }
    }
}
